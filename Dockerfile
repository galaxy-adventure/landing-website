FROM node:14-alpine

# Expose port
EXPOSE 3000/tcp

# Work directory
WORKDIR /app

# Add package.json and install 'dependencies' only
ADD package.json /app/
RUN npm install --only=prod

# ADD source code
ADD . /app

# Build source
RUN npm run build

CMD ["npx", "next", "start", "-p", "3000"]
