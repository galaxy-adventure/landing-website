import React from "react";

export default function Banner() {
  return (
    <div className="bg-container">
      <a
        href="https://app.galaxyadventure.io"
        target="_blank"
        rel="noreferrer"
        id="btn-getting-started"
      >
        <input type="button" value="Play Now" />
      </a>
      <a
        href="https://pancakeswap.finance/swap?inputCurrency=BNB&outputCurrency=0x292Bb969737372E48D97C78c19B6a40347C33B45"
        target="_blank"
        rel="noreferrer"
        id="btn-buy-token"
      >
        <input type="button" value="Buy $GLA" />
      </a>
    </div>
  );
}
