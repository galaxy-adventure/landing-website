import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

export default function Roadmap() {
    return (
        <div className="pricing-section pt-80" id="roadmap">
            <Container>
                <Row className="justify-content-center">
                    <Col lg={8} className="text-center">
                        <div className="section-header">
                            <h2 className="section-title">ROAD MAP</h2>
                        </div>
                    </Col>
                </Row>
                <div className="pricing-item-area">
                    <Row className="justify-content-center align-items-stretch mb-30">
                        <Col lg={5} md={4} sm={8} className="text-center">
                            <span
                                className="roadmap-title"
                                style={{ color: '#FFAE10' }}
                            >
                                Marketing
                            </span>
                        </Col>
                        <Col lg={2} md={4} sm={8}></Col>
                        <Col lg={5} md={4} sm={8} className="text-center">
                            <span
                                className="roadmap-title"
                                style={{ color: '#1BD766' }}
                            >
                                Game
                            </span>
                        </Col>
                    </Row>

                    <Row className="justify-content-center align-items-stretch mb-30">
                        <Col
                            lg={5}
                            md={4}
                            sm={8}
                            className="mb-30 order-2 order-md-1"
                        >
                            <div
                                className="pricing-item d-flex align-items-center justify-content-end"
                                style={{
                                    background: 'rgba(255, 255, 255, 0.3)',
                                }}
                            >
                                <ul
                                    className="pricing-list"
                                    style={{ direction: 'rtl' }}
                                >
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked2.png"
                                            alt=""
                                        />
                                        <span>Launch Social + Website</span>
                                    </li>
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked2.png"
                                            alt=""
                                        />
                                        <span>Whitelist sale + Airdrop</span>
                                    </li>

                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked2.png"
                                            alt=""
                                        />
                                        <span>
                                            Marketing / IGO on Oxbull
                                        </span>
                                    </li>

                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked2.png"
                                            alt=""
                                        />
                                        <span>
                                            Public Launch On PancakeSwap
                                        </span>
                                    </li>
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked2.png"
                                            alt=""
                                        />
                                        <span>
                                            Coingecko, Coinmarketcap Listing
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col
                            lg={2}
                            md={4}
                            sm={8}
                            className="mb-30 align-self-center text-center order-1 order-md-2"
                        >
                            <img
                                src="assets/img/phase1.png"
                                alt="phase1"
                                width="80%"
                                className="img-phase"
                            />
                        </Col>
                        <Col
                            lg={5}
                            md={4}
                            sm={8}
                            className="mb-30 order-3 order-md-3"
                        >
                            <div
                                className="pricing-item d-flex align-items-center justify-content-start"
                                style={{
                                    background: 'rgba(255, 255, 255, 0.3)',
                                }}
                            >
                                <ul className="pricing-list">
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked1.png"
                                            alt=""
                                        />
                                        <span>
                                            Design website, game characters and graphics
                                        </span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked1.png"
                                            alt=""
                                        />
                                        <span>
                                            Smart contract and game base
                                        </span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked1.png"
                                            alt=""
                                        />
                                        <span>Audit Contract</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked1.png"
                                            alt=""
                                        />
                                        <span>Public Launch NFT Battle</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked1.png"
                                            alt=""
                                        />
                                        <span>
                                            Public Launch NFT Marketplace
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>

                    <Row className="justify-content-center align-items-stretch mb-30">
                        <Col lg={5} md={4} sm={8} className="mb-30 order-2 order-md-1">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-end"
                                style={{
                                    background: 'rgba(34, 166, 151, 0.78)',
                                }}
                            >
                                <ul
                                    className="pricing-list"
                                    style={{ direction: 'rtl' }}
                                >
                                    <li className="d-flex align-items-center">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>CEX Listing</span>
                                    </li>
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>
                                            Influencer Marketing and Social
                                            Media Promotions
                                        </span>
                                    </li>
                                    <li className="d-flex align-items-center">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Expanding Team</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col
                            lg={2}
                            md={4}
                            sm={8}
                            className="mb-30 align-self-center text-center order-1 order-md-2"
                        >
                            <img
                                src="assets/img/phase2.png"
                                alt="phase2"
                                width="80%"
                                className="img-phase"
                            />
                        </Col>
                        <Col lg={5} md={4} sm={8} className="mb-30 order-3 order-md-3">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-start"
                                style={{
                                    background: 'rgba(34, 166, 151, 0.78)',
                                }}
                            >
                                <ul className="pricing-list">
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Release the GALAXY MAP</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Release ITEMS enhancements</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Release ITEMS upgrade</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Introduce DROIDS</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>
                                            Release MULTI-HERO BATTLE Mode
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>

                    <Row className="justify-content-center align-items-stretch mb-30">
                        <Col lg={5} md={4} sm={8} className="mb-30 order-2 order-md-1">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-end"
                                style={{
                                    background: 'rgba(58, 125, 255, 0.6)',
                                }}
                            >
                                <ul
                                    className="pricing-list"
                                    style={{ direction: 'rtl' }}
                                >
                                    <li className="d-flex align-items-center">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span className="text-right">
                                            Partnerships with Game Studios
                                        </span>
                                    </li>
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Listing on more exchanges</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col
                            lg={2}
                            md={4}
                            sm={8}
                            className="mb-30 align-self-center text-center order-1 order-md-2"
                        >
                            <img
                                src="assets/img/phase3.png"
                                alt="phase1"
                                width="80%"
                                className="img-phase"
                            />
                        </Col>
                        <Col lg={5} md={4} sm={8} className="mb-30 order-3 order-md-3">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-start"
                                style={{
                                    background: 'rgba(58, 125, 255, 0.6)',
                                }}
                            >
                                <ul className="pricing-list">
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>NFT Real Estate Trading</span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>
                                            New Game Mode: Build your Empire
                                        </span>
                                    </li>
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>New Staking Methods</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>

                    <Row className="justify-content-center align-items-stretch mb-30">
                        <Col lg={5} md={4} sm={8} className="mb-30 order-2 order-md-1">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-end"
                                style={{
                                    background: 'rgba(255, 211, 55, 0.6)',
                                }}
                            >
                                <ul
                                    className="pricing-list"
                                    style={{ direction: 'rtl' }}
                                >
                                    <li className="d-flex align-items-center text-right">
                                        <img
                                            className="icon-right"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Biggest exchanges listing</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                        <Col
                            lg={2}
                            md={4}
                            sm={8}
                            className="mb-30 align-self-center text-center order-1 order-md-2"
                        >
                            <img
                                src="assets/img/phase4.png"
                                alt="phase1"
                                width="80%"
                                className="img-phase"
                            />
                        </Col>
                        <Col lg={5} md={4} sm={8} className="mb-30 order-3 order-md-3">
                            <div
                                className="pricing-item d-flex align-items-center justify-content-start"
                                style={{
                                    background: 'rgba(255, 211, 55, 0.6)',
                                }}
                            >
                                <ul className="pricing-list">
                                    <li>
                                        <img
                                            className="icon-left"
                                            width="20"
                                            src="/assets/img/checked3.png"
                                            alt=""
                                        />
                                        <span>Release Metaverse</span>
                                    </li>
                                </ul>
                            </div>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    );
}
