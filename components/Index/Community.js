import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Image from "next/image";

export default function Community() {
    return (
        <div className="footer-section" id="community">
            <Container>
                <Row>
                    <Col>
                        <div className="footer-content">
                            <img
                                src="/assets/img/icon.png"
                                alt="element"
                                className="icon_war"
                            />
                            <h3 className="sub-title">GET IN TOUCH</h3>
                            <h2 className="title">Join our community</h2>
                            <ul className="footer-social-icon">
                                <li>
                                    <a
                                        href="https://t.me/GalaxyAdventureIO"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <Image
                                            src="/assets/img/icon/icon_tele.png"
                                            alt="element"
                                            width="196"
                                            height="200"
                                        />
                                    </a>
                                </li>
                                {/* <li>
                  <a href="#" target="_blank" rel="noreferrer">
                    <img
                      src="/assets/img/icon/icon_youtube.png"
                      alt="element"
                    />
                  </a>
                </li> */}
                                <li>
                                    <a
                                        href="https://www.reddit.com/user/galaxy_adventure"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <Image
                                            src="/assets/img/icon/icon_reddit.png"
                                            alt="element"
                                            width="196"
                                            height="200"
                                        />
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://twitter.com/galaxynftgame"
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        <Image
                                            src="/assets/img/icon/icon_twitter.png"
                                            alt="element"
                                            width="196"
                                            height="200"
                                        />
                                    </a>
                                </li>
                            </ul>
                            <div className="contact">Contact Us: <span className="mail">support@galaxyadventure.io</span></div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
