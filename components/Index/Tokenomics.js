import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';

export default function Client() {
    return (
        <div className="client-section pt-80 pb-120" id="tokenomics">
            <Container>
                <Row className="justify-content-center align-items-center mb-30-none">
                    <Col lg={10} className="text-center">
                        <div className="section-header">
                            <h2 className="section-title">TOKENOMICS</h2>
                        </div>
                        <img src="/assets/img/tokenomics.png" />
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
