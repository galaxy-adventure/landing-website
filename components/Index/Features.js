import React, {useEffect, useState} from "react";
import { Col, Container, Row }      from "react-bootstrap";
import Image                        from "next/image";
import Link                         from "next/link";
import {HERO_ABI}                   from "../../config/abi";
import {numberSeparator}            from "../../utils/number"

export default function About() {
    const [price, setPrice] = useState(0.0024);
    const [nftHero, setNftHero] = useState(0);
    const [remainNft, setRemainNft] = useState(150000);

    useEffect(async => {
        fetch("https://api.pancakeswap.info/api/v2/tokens/0x292bb969737372e48d97c78c19b6a40347c33b45")
        .then(res => res.json())
        .then(
            (result) => {
                // let priceUsd = result.data.price*10**18;
                let priceUsd = result.data.price * 1;
                setPrice(priceUsd.toFixed(5));
            },
            (error) => {
                console.log(error);
            }
        )
    })

    useEffect(async => {
        const requestOptions = {
            method: 'POST',
            headers: { 'accept': 'application/json', 'X-API-Key' : 'ifp9IpMADjYndVFmVKUfnfjptEAaS7qfL1HS2GfGc7q7HvqxD4Mpok8Ob1rPReen', 'Content-Type': 'application/json' },
            body: JSON.stringify(HERO_ABI)
        };
        fetch("https://deep-index.moralis.io/api/v2/0xD7d03512Ef86Ec24331c1C6455f879011b57b85d/function?chain=bsc&function_name=totalSupply", requestOptions)
        .then(res => res.json())
        .then(
            (result) => {
                setNftHero(result);
                setRemainNft(150000 - result);
            },
            (error) => {
                console.log(error);
            }
        )
    })

    return (
        <div className="about-section pt-80 pb-80" id="features">
            <Container>
                <Row className="justify-content-center mb-6">
                    <Col
                        lg={4}
                        md={6}
                        xs={4}
                        className="mb-30 d-flex justify-content-center"
                    >
                        <div className="thumbnail">
                            <Image
                                priority
                                src="/assets/img/nftgroup.png"
                                height={394}
                                width={394}
                                alt="Price"
                            />
                            <div className="caption caption-number">
                                ${price}
                            </div>
                            <div className="caption caption-title">PRICE</div>
                        </div>
                    </Col>
                    <Col
                        lg={4}
                        md={6}
                        xs={4}
                        className="mb-30 d-flex justify-content-center"
                    >
                        <div className="thumbnail">
                            <Image
                                priority
                                src="/assets/img/nftgroup.png"
                                height={394}
                                width={394}
                                alt="NFT Amount"
                            />
                            <div className="caption caption-number">{nftHero}</div>
                            <div className="caption caption-title">
                                TOTAL NFT MINTED
                            </div>
                        </div>
                    </Col>
                    <Col
                        lg={4}
                        md={6}
                        xs={4}
                        className="mb-30 d-flex justify-content-center"
                    >
                        <div className="thumbnail">
                            <Image
                                priority
                                src="/assets/img/nftgroup.png"
                                height={394}
                                width={394}
                                alt="NFT's remaining"
                            />
                            <div className="caption caption-number">
                                {numberSeparator(remainNft)}
                            </div>
                            <div className="caption caption-title">
                                NFT's REMAINING
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row className="justify-content-center align-items-center pt-80 mb-30-none">
                    <Col lg={10} className="text-center">
                        <div className="thumbnail">
                            <img
                                className="story-bg"
                                style={{ paddingBottom: 30 }}
                                src="/assets/img/story.png"
                            />
                            <div className="caption ourstory-title">
                                <img
                                    className="ourstory-title-img"
                                    src="/assets/img/ourstory.png"
                                    width={340}
                                    height={40}
                                    alt="Our story"
                                />
                            </div>
                            <div className="video-block">
                                <iframe
                                    className="responsive-iframe"
                                    src="https://www.youtube.com/embed/FYW5RsMdhKw??autoplay=0&mute=1"
                                />
                            </div>
                            <div className="ourstory-block">
                                <p className="responsive-ourstory-block">
                                    The galaxy is under attack! Humans need to
                                    find the Cryptonian Crystals and unlock
                                    their unlimited power! Why? Aliens are
                                    terraforming different planets, and Earth is
                                    next on their list! This looks like a job
                                    for Earth’s Mightiest Heroes! Fend off the
                                    Glorgonite army, battle the WORLD ENDERS,
                                    and save Earth from destruction!!
                                </p>
                            </div>
                            <Link href="https://docs.galaxyadventure.io/introduction/story" passHref>
                                <div
                                    id="btn-read-more"
                                    className="d-none d-md-block"
                                >
                                    <input
                                        type="button"
                                        width="196"
                                        height="78"
                                        value="Read more"
                                    />
                                </div>
                            </Link>
                            <Link href="https://docs.galaxyadventure.io/introduction/story" passHref>
                                <div id="btn-read-more" className="d-md-none">
                                    <input
                                        type="button"
                                        width="196"
                                        height="78"
                                        value="Story"
                                    />
                                </div>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    );
}
