import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import Image from "next/image";

export default function Challenge() {
  return (
    <div className="about-section pt-80 pb-80" id="howtoplay">
      <Container>
        <div className="challenge-area">
          <Row className="justify-content-center mb-30">
            <Col className="text-center">
              <h2 className="section-title">PLAY-TO-EARN FEATURES</h2>
            </Col>
          </Row>
          <Row className="justify-content-center mb-30-none">
            <Col lg={2} md={6} sm={12} className="mb-30">
              <div className="challenge-item text-center">
                <div className="challenge-thumb">
                  <Image
                    src="/assets/img/play/play01.png"
                    height={724}
                    width={222}
                    alt="Training"
                  />
                </div>
              </div>
            </Col>
            <Col lg={2} md={6} sm={12} className="mb-30">
              <div className="challenge-item text-center">
                <div className="challenge-thumb">
                  <Image
                    src="/assets/img/play/play02.png"
                    height={724}
                    width={222}
                    alt="Evolving"
                  />
                </div>
              </div>
            </Col>
            <Col lg={2} md={6} sm={12} className="mb-30">
              <div className="challenge-item text-center">
                <div className="challenge-thumb">
                  <Image
                    src="/assets/img/play/play03.png"
                    height={724}
                    width={222}
                    alt="Rewarding"
                  />
                </div>
              </div>
            </Col>
            <Col lg={2} md={6} sm={12} className="mb-30">
              <div className="challenge-item text-center">
                <div className="challenge-thumb">
                  <Image
                    src="/assets/img/play/play04.png"
                    height={724}
                    width={222}
                    alt="PVP"
                  />
                </div>
              </div>
            </Col>
            <Col lg={2} md={6} sm={12} className="mb-30">
              <div className="challenge-item text-center">
                <div className="challenge-thumb">
                  <Image
                    src="/assets/img/play/play05.png"
                    height={724}
                    width={222}
                    alt="PVP"
                  />
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Container>
    </div>
  );
}
