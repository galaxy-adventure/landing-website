import React from "react";
import { Container } from "react-bootstrap";

export default function Footer() {
  return (
    <footer>
      <Container>
        <div className="copyright-area">
          <div className="copyright-content">
            <p>
              Copyright © 2021. All Rights Reserved By{" "}
              <a href="#">Galaxy Adventure</a>
            </p>
          </div>
        </div>
      </Container>
    </footer>
  );
}
