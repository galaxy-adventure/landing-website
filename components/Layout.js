import React from 'react';
import Header from './Header';
import Footer from './Footer';
import Head from 'next/head';

export default function Layout({ children }) {
    return (
        <div className="main-app">
            <Head>
                <title>Galaxy Adventure</title>
                <meta
                    name="viewport"
                    content="initial-scale=1.0, width=device-width"
                />
                <link
                    rel="icon"
                    type="image/png"
                    sizes="400x400"
                    href="/assets/img/favicon/icon.png"
                />
                <link
                    rel="manifest"
                    href="/assets/img/favicon/site.webmanifest"
                />
                <link
                    rel="mask-icon"
                    href="/assets/img/favicon/fullbg2.svg"
                    color="#5bbad5"
                />
                <meta name="msapplication-TileColor" content="#da532c" />
                <meta name="theme-color" content="#ffffff" />
            </Head>
            <Header />
            {children}
            <Footer />
        </div>
    );
}
