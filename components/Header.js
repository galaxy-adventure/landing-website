import React, { useEffect, useRef } from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';

export default function Header() {
    const navRef = useRef(null);

    useEffect(() => {
        window.onscroll = () => {
            if (window.scrollY > 500) {
                navRef.current.classList.add(
                    'animated',
                    'fadeInDown',
                    'header-fixed'
                );
            } else {
                navRef.current.classList.remove(
                    'animated',
                    'fadeInDown',
                    'header-fixed'
                );
            }
        };

        let menuItems = document.querySelectorAll('.navbar li a');

        menuItems.forEach((item, index) => {
            item.onclick = () => {
                let element = item.parentElement;
                if (element.classList.contains('show')) {
                    element.classList.remove('show');
                    element.querySelectorAll('li').forEach((tag, index) => {
                        tag.classList.remove('show');
                    });
                } else {
                    element.classList.add('show');
                    element.querySelectorAll('li').forEach((tag, index) => {
                        tag.classList.remove('show');
                    });
                }
            };
        });
    });

    return (
        <div className="header-section" id="header" ref={navRef}>
            <div className="header">
                <div className="header-top-area">
                    <Container>
                        <div className="header-top-content d-flex flex-wrap align-items-center justify-content-between">
                            <div className="header-overlay">
                                <div className="header-logo">
                                    <a
                                        className="site-logo site-title"
                                        href="#"
                                    >
                                        <img
                                            src="/assets/img/icon.png"
                                            alt="site-logo"
                                        />
                                    </a>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
                <div className="header-bottom">
                    <Container>
                        <Navbar collapseOnSelect expand="lg" className="p-0">
                            <a
                                className="site-logo site-title d-lg-none"
                                href="#"
                            >
                                <img
                                    src="assets/img/icon.png"
                                    alt="site-logo"
                                />
                            </a>
                            <Navbar.Toggle aria-controls="responsive-navbar-nav">
                                <span className="fas fa-bars"></span>
                            </Navbar.Toggle>
                            <Navbar.Collapse id="responsive-navbar-nav">
                                <Nav className="mr-auto">
                                    <ul className="navbar-nav main-menu">
                                        <li className="active">
                                            <a href="#">Home</a>
                                        </li>
                                        <li>
                                            <a href="#features">Our Story</a>
                                        </li>
                                        <li>
                                            <a href="#howtoplay">
                                                Play-to-earn
                                            </a>
                                        </li>
                                        <li>
                                            <a href="#tokenomics">Tokenomics</a>
                                        </li>
                                        <li>
                                            <a href="#roadmap">Road Map</a>
                                        </li>
                                        <li>
                                            <a href="#partners">Partners</a>
                                        </li>
                                        <li>
                                            <a href="#community">Community</a>
                                        </li>
                                        <li className="menu_has_children">
                                            <a>
                                                More
                                                <i
                                                    className="fa fa-caret-down"
                                                    aria-hidden="true"
                                                ></i>
                                            </a>
                                            <div className="sub-menu">
                                                <ul className="sub-menu-child">
                                                    <li>
                                                        <a href="https://docs.galaxyadventure.io">
                                                            Whitepaper
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </Container>
                </div>
            </div>
        </div>
    );
}
