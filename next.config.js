module.exports = {
  reactStrictMode: true,
  images: {
    loader: 'imgix',
    path: 'https://galaxyadventure.io',
  },
}
