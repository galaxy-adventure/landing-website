import Banner from "../components/Index/Banner";
import Community from "../components/Index/Community";
import Features from "../components/Index/Features";
import HowToPlay from "../components/Index/HowToPlay";
import Partners from "../components/Index/Partners";
import Roadmap from "../components/Index/Roadmap";
import Tokenomics from "../components/Index/Tokenomics";

export default function Home() {
  return (
    <div className="main">
      <Banner></Banner>
      <div className="content-wrapper">
        <Features></Features>
        <HowToPlay></HowToPlay>
        <Tokenomics></Tokenomics>
        <Roadmap></Roadmap>
        <Partners></Partners>
        <Community></Community>
      </div>
    </div>
  );
}
