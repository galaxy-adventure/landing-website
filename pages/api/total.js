// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

import {TOKEN_ABI} from "../../config/abi";

export default async function handler(req, res) {
    const requestOptions = {
        method: 'POST',
        headers: { 'accept': 'application/json', 'X-API-Key' : 'ifp9IpMADjYndVFmVKUfnfjptEAaS7qfL1HS2GfGc7q7HvqxD4Mpok8Ob1rPReen', 'Content-Type': 'application/json' },
        body: JSON.stringify(TOKEN_ABI)
    };
   fetch("https://deep-index.moralis.io/api/v2/0x292bb969737372e48d97c78c19b6a40347c33b45/function?chain=bsc&function_name=totalSupply", requestOptions)
        .then(res => res.json())
        .then(
            (result) => {
                res.status(200).json(parseFloat(result) / (10 ** 18))
            },
            (error) => {
                res.status(500).json(error)
            }
        )
}
