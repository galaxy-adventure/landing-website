import Layout from '../components/Layout';

import 'bootstrap/dist/css/bootstrap.min.css';
import '../public/assets/css/fontawesome-all.min.css';
import '../public/assets/css/animate.css';
import '../styles/globals.css';
import '../public/assets/css/responsive.css';
import '../styles/custom.css';

function MyApp({ Component, pageProps }) {
    return (
        <Layout>
            <Component {...pageProps} />
        </Layout>
    );
}

export default MyApp;
